using UnityEngine;

public class HeaderPanel : MonoBehaviour
{
    private WebViewObject webViewObject;
    
    public void ClickBackBtn()
    {
        webViewObject.GoBack();
    }

    public void ClickForwardBtn()
    {
        webViewObject.GoForward();
    }

    public void ClickRefreshBtn()
    {
        webViewObject.Reload();
    }

    public void ClickCloseBtn()
    {
        Hide();
    }

    public void Show(WebViewObject viewObject)
    {
        gameObject.SetActive(true);
        this.webViewObject = viewObject;
    }

    private void Hide()
    {
        var webView = GameObject.Find("WebViewObject");
        if (webView != null)
        {
            Destroy(webView);
        }
        gameObject.SetActive(false);
    }
}
