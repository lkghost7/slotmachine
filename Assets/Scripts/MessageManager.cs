using System;
using UnityEngine;

public class MessageManager : GenericSingletonClass<MessageManager>
{
    [SerializeField] private Transform messageParent;
    [SerializeField] private Message messagePrefab;
    public Action<string> MessageAction { get; private set; }

    private void Start()
    {
        MessageAction += ShowMessage;
    }

    private void ShowMessage(string message)
    {
        Message messageItem = Instantiate(messagePrefab, messageParent);
        messageItem.ShowMessage(message);
    }
}
