using System;

[Serializable] 

public class StatusItem
{ 
    public string name;
    public int status;
    
    public StatusItem(string name, int status)
    {
        this.name = name;
        this.status = status;
    }
    
    public StatusItem()
    {
    }
}