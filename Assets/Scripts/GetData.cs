using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class GetData : MonoBehaviour
{
    private string urlStatus = "https://redir-app-dg98f.ondigitalocean.app";

    private IEnumerator SendRequestStatus(Action<UnityWebRequest, StatusItem> callback)
    {
        List<StatusItem> StatusList  = new List<StatusItem>();
        UnityWebRequest request = UnityWebRequest.Get(urlStatus);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log($"{request.error}: {request.downloadHandler.text}");
        }
        else
        {
            Dictionary<string, object> objects = JsonConvert.DeserializeObject<Dictionary<string, object>>(request.downloadHandler.text);

            foreach (KeyValuePair<string, object> keyValuePair in objects)
            {
                StatusItem statusItem = new StatusItem();
                statusItem.name = keyValuePair.Key;

                try
                {
                    statusItem.status = Convert.ToInt32(keyValuePair.Value);
                }
                catch (Exception e)
                {
                    Debug.Log($"Chek data file, error {e}");
                    throw;
                }
                StatusList.Add(statusItem);
            }
        }
        
        callback.Invoke(request, StatusList[0]);
    }

    public void GetStatus(Action<UnityWebRequest, StatusItem> callBack)
    {
        StartCoroutine(SendRequestStatus(callBack));
    }
}