using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Message : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI messageText;
    private CanvasGroup canvas;

    public void ShowMessage(string text)
    {
        canvas = GetComponent<CanvasGroup>();
        messageText.text = text;
        canvas.alpha = 0;
        canvas.DOFade(1, 0.45f).OnComplete(() =>
        {
            StartCoroutine(DelayMessage());
        });
    }
    
    IEnumerator DelayMessage()
    {
        yield return new WaitForSecondsRealtime(1.5f);
        canvas.DOFade(0, 0.45f).OnComplete(() =>
        {
            Destroy(gameObject);
        });
    }
}
