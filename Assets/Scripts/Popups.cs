using UnityEngine;

public class Popups : MonoBehaviour
{
    public void HideBtn()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}
