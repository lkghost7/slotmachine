using System.Collections;
using UnityEngine;

public class WebViewControl : MonoBehaviour
{
    [Header("Config")]
    [SerializeField] [Tooltip("url for start")] private string url;
    [SerializeField] private HeaderPanel headerPanel;
    
    private WebViewObject webViewObject;
    IEnumerator InitWebPage()
    {
        webViewObject = new GameObject("WebViewObject").AddComponent<WebViewObject>();
        webViewObject.Init();
        webViewObject.SetMargins(0, 60, 0, 0);
        webViewObject.SetTextZoom(100);
        webViewObject.SetVisibility(true);
        webViewObject.LoadURL(url);
        
        headerPanel.Show(webViewObject);
        
        yield break;
    }

    public void ClickStartPage()
    {
        StartCoroutine(InitWebPage());
    }
}