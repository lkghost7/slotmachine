using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class Drum : MonoBehaviour
{
    public bool Chek { get; set; }
    public int ID { get; private set; }

    private readonly List<int> angleList = new List<int>();
    private readonly List<int> circleList = new List<int>();
    public int winNum { get; private set; }
    public bool freezeRotation { get; private set; }

    private void Start()
    {
        InitDrumData(30, 12, angleList);
        InitDrumData(360, 7, circleList);
    }

    private void InitDrumData(int step, int circle, List<int> dataList)
    {
        int currentStep = 0;

        for (int i = 0; i < circle; i++)
        {
            currentStep += step;
            dataList.Add(currentStep);
        }
    }

    public void DrumRotate(int id, Action spinUnlock, float timeSpin)
    {
        ID = id;
        freezeRotation = true;
        Chek = false;

        int indexAngle = Random.Range(0, angleList.Count);
        int indexCircle = Random.Range(0, circleList.Count);
        winNum = indexAngle;
        int rangeAngle = angleList[indexAngle];
        int rangeCircle = circleList[indexCircle];
        rangeAngle += rangeCircle;

        transform.DOLocalRotate(new Vector3(0, rangeAngle, 0), timeSpin, RotateMode.FastBeyond360).SetEase(Ease.OutQuart).onComplete = () =>
        {
            freezeRotation = false;
            spinUnlock.Invoke();
        };
    }
}