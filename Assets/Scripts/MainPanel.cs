using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MainPanel : MonoBehaviour
{
    [SerializeField] private Text status;
    [SerializeField] private GetData getData;
    [SerializeField] private Button chekStatusBtn;
    [SerializeField] private Button startBtn;

    [SerializeField] private Popups disconnectPopup;
    [SerializeField] private Popups statusOnePopup;

    private CanvasGroup canvasGroup;
    private Action<UnityWebRequest, StatusItem> connectStatusStartBtn;
    private Action<UnityWebRequest, StatusItem> connectStatusStart;
    private Action<UnityWebRequest, StatusItem> connectStatusCheck;

    private void Start()
    {
        connectStatusStartBtn += StartGameCallBack;
        connectStatusStart += ShowStatusStart;
        connectStatusCheck += ShowStatus;
        chekStatusBtn.interactable = true;

        canvasGroup = GetComponent<CanvasGroup>();
        getData.GetStatus(connectStatusStart);
    }

    public void StartGame()
    {
        getData.GetStatus(connectStatusStartBtn);
    }

    public void ForceLoadGame()
    {
        disconnectPopup.HideBtn();
        statusOnePopup.HideBtn();
        canvasGroup.DOFade(0, 1).OnComplete(() => { gameObject.SetActive(false); });
    }

    private void StartGameCallBack(UnityWebRequest request, StatusItem statusItem)
    {
        chekStatusBtn.interactable = false;
        startBtn.interactable = false;

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            disconnectPopup.Show();
            chekStatusBtn.interactable = true;
            startBtn.interactable = true;
            return;
        }

        if (statusItem.status == 0)
        {
            canvasGroup.DOFade(0, 1).OnComplete(() => { gameObject.SetActive(false); });
        }
        else
        {
            statusOnePopup.Show();
        }

        startBtn.interactable = true;
        chekStatusBtn.interactable = true;
    }

    public void CheckStatus()
    {
        getData.GetStatus(connectStatusCheck);
    }

    private void ShowStatus(UnityWebRequest request, StatusItem statusItem)
    {
        startBtn.interactable = false;
        chekStatusBtn.interactable = false;

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            status.text = "connect fail";
            chekStatusBtn.interactable = true;
            startBtn.interactable = true;
            return;
        }

        status.text = statusItem.name + " : " + statusItem.status;
        chekStatusBtn.interactable = true;
        startBtn.interactable = true;
    }


    private void ShowStatusStart(UnityWebRequest request, StatusItem statusItem)
    {
        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            status.text = "connect fail";
            return;
        }

        status.text = statusItem.name + " : " + statusItem.status;
    }

    public void Show()
    {
        gameObject.SetActive(true);
        startBtn.interactable = true;
        chekStatusBtn.interactable = true;
        canvasGroup.alpha = 0;
        canvasGroup.DOFade(1, 1);
    }

    public void ExitBtn()
    {
        Application.Quit();
    }
}