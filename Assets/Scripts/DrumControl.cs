using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class DrumControl : MonoBehaviour
{
    [Header("Config")] [SerializeField] [Tooltip("Reel spin time")]
    private float spinTime = 3.5f;

    [SerializeField] private Drum[] drums;
    [SerializeField] private Button spinBtn;
    private readonly List<string> colorList = new List<string>();
    private Action unlockSpinAction;

    private void Start()
    {
        unlockSpinAction += UnlockSpin;
        InitColors();
    }

    public void StartDrums()
    {
        spinBtn.interactable = false;
        for (var i = 0; i < drums.Length; i++)
        {
            Drum drum = drums[i];
            if (drum.freezeRotation)
            {
                return;
            }

            drum.DrumRotate(i, unlockSpinAction, spinTime);
        }
        ChekWins();
    }

    private void ChekWins()
    {
        WinItem[] wins = ChekWinCombination(drums);
        StringBuilder sb = new StringBuilder();
        sb.Append("You wins ! ");
        int count = 0;

        for (var i = 0; i < wins.Length; i++)
        {
            if (wins[i].winCount > 0)
            {
                count++;
                sb.Append(wins[i].winCount + 1 + " - ");
                sb.Append(wins[i].color + "  ");
            }
        }

        if (count == 0)
        {
            sb.Clear();
            sb.Append("You lose !");
        }
        
        StartCoroutine(DelayMessage(sb.ToString()));
    }

    private IEnumerator DelayMessage(string message)
    {
        yield return new WaitForSecondsRealtime(spinTime - 0.5f);
        MessageManager.Instance.MessageAction.Invoke(message);
    }

    private void UnlockSpin()
    {
        spinBtn.interactable = true;
    }

    private WinItem[] ChekWinCombination(Drum[] drumsList)
    {
        WinItem[] numsCircle = new WinItem[drumsList.Length];

        for (int i = 0; i < drumsList.Length; i++)
        {
            int result = 0;
            int winNum = drumsList[i].winNum;

            for (int j = 0; j < drumsList.Length; j++)
            {
                if (drumsList[j].ID == drumsList[i].ID)
                {
                    continue;
                }

                if (drumsList[j].Chek)
                {
                    continue;
                }

                if (drumsList[j].winNum == winNum)
                {
                    drumsList[j].Chek = true;

                    numsCircle[i].color = colorList[winNum];
                    result++;
                }
            }

            numsCircle[i].ID = i;
            numsCircle[i].winCount += result;
            drumsList[i].Chek = true;
        }

        return numsCircle;
    }
    
    private void InitColors()
    {
        colorList.Add("Bell");
        colorList.Add("Strawberry");
        colorList.Add("Seven"); 
        colorList.Add("Bar");
        colorList.Add("Watermelon"); 
        colorList.Add("Grapes"); 
        colorList.Add("Chery"); 
        colorList.Add("Banana"); 
        colorList.Add("Plum");
        colorList.Add("Lemon"); 
        colorList.Add("Orange");
        colorList.Add("Star"); 
    }
}